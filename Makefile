INCDIR= -Iinclude -I/usr/include
LIBDIR= -Llib -L/usr/lib64
LIBS=    -lusb

default: main


%.o : %.cc %.hh
	g++  -c $(INCDIR) $< -o $@


main :  UsbDevice.o main.o
	g++  $(LIBDIR) $(LIBS) $< -o $@ main.o   

clean: 
	-rm  *.o
	
