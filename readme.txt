This code should be used to reprogram the FPGA design.
See the documentation to know how it is working.
To reprogram the design use program like this:
./main -p new_design.rpd   (to program the update)
./main -h   (help)
./main -v design.rpd
