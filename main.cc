#include "UsbDevice.hh"
#include <stdio.h>
#include <string.h>
#include <usb.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <stdint.h>
#include <time.h>


using namespace std;

#define uInt32 uint32_t

class g_USBDevice {
	public :
                UsbDevice&  dev() { return *gdev; }
                g_USBDevice() { gdev=NULL; }
                ~g_USBDevice() {}//if (gdev) delete gdev;}
                void Open (uInt32 VENDOR_ID,uInt32 DEVICE_ID) 
		{
		cout << hex << VENDOR_ID << endl;
               if (gdev) {
		        //delete gdev;
	                }
               gdev= new UsbDevice(VENDOR_ID,DEVICE_ID);
               }
                void Close () {if (gdev) 
		{
                 delete gdev;
                 }
		}
	private:
		UsbDevice *gdev;
	
} gdev;



void OpenDevice(uInt32 VENDOR_ID, uInt32 DEVICE_ID)
{
  
   gdev.Open(VENDOR_ID,DEVICE_ID);
  
}

void CloseDevice()
{
 	 gdev.Close();
}


uInt32 WriteDevice (uInt32 address,
		    uInt32 length,
		    uInt32 buffer[512])
{
  	int len;
	len = gdev.dev().write(address,length,buffer);
	//cout << len <<endl;
	 return(len/4);
}

uInt32 ReadDevice (uInt32 address,
	    	   uInt32 length,
	   	   uInt32 buffer[512])
{
 	 int len;
	len =  gdev.dev().read(address,length, buffer);
	// cout << len << endl;
		     return(len/4);
}

uInt32 BulkRead (uInt32 pipe,
		uInt32 length,
		uInt32 buffer[512])
{
	int len;
	len = gdev.dev().Read_Bulk(pipe,length,buffer);
	//cout << "bulk len " << dec << len << endl;
	 return(len);
}
 

int main(int argc,char *argv[])
{
 int i,j,k;
 int end;
 int purcent;
 char *name,byte_c,*param;
 int byte,error=0;
 const string prog = "-p" ;
 const string verif = "-v";
 //time_t timer;
 ifstream fd;
 uint32_t word;
 uint32_t buffer_w[512];
 uint32_t page_sect[2];
 uint32_t buffer_r[512];


 if (argc <= 2) {
	cout << "You should specify a rpd file to be programmed !! " << endl;
	cout << "-h  : for help " << endl;
	cout << "-p  : to program the Flash, a .rpd file should be provided" << endl;
	cout << "-v  : to verify the flash, a .rpd file shoud be provided" << endl;
        cout << "Ex: ./main -p file_to_prog.rpd " << endl;
	cout << endl;
	return 0;
 }

 param = argv[1];
 name = argv[2];

 string temp(name,strlen(name));
 string extension=temp.substr(temp.length()-4,temp.length()-1);
 if (extension != ".rpd") {
	cout << "The file you specify is not a file to program the FPGA flash!!!! " << endl;
	cout << endl;
	return 0;
	}


 fd.open(name, ios::in | ios::binary);
 if (!fd) {
	cout << "The file you specified doesn't exist !! " << endl;
	return 0;
	}
 if (param == prog) {

	 OpenDevice(0x1556,0x0420);
	  
	 cout << "Start to erase the flash section 0x300000!" << endl;
	 for (i= 0;i < 41;i++)
		{
		page_sect[0] = 0x300000 + (i * 0x10000); // set the sector to erase
		WriteDevice (0XE000,1,page_sect);        // send the address
		WriteDevice (0XE00C,1,buffer_w);        // send the request erase
	
		end = 1;
		while (end == 1)  //loop untilthe erase is done
			{
			 ReadDevice (0xE014,1,buffer_r);
			 end = buffer_r[0] & 0x1;
			}

		purcent = int((100 * i)/41);
		cout << dec << purcent << "%" << "\r" ;	
		cout.flush();
		} 

	cout << "\xd" << "100%"  << endl;
 	cout << "Start to program the new design " << endl;

	 for (i=0 ;i < 10284 ;i++) // number of page 10284
		{
		//prepare a page
		for (j=0 ;j<64 ;j++)
			{
			word = 0;
			for (k=0;k<4;k++)
				{
				if (fd.get(byte_c)) {
					//cout << hex << int(byte_c) << endl;
					byte = byte_c & 0xFF;
				 	word = word + (byte * (1<<(k*8)) );
					} else {
					word = word + (0xFF * (1<<(k*8)) );
					}
				}
			buffer_w[j] = word;
			}

		// set the address to be written
		page_sect[0] = 0x300000 + (i * 0x100); // set the page to write
		WriteDevice (0XE000,1,page_sect);        // send the address
		// send the page to be written
		WriteDevice (0XE008,64,buffer_w);         
		end = 1;
		while (end == 1)  //loop untilthe program is done
			{
			 ReadDevice (0xE014,1,buffer_r);
			 end = buffer_r[0] & 0x1;
			}

		purcent = int((100 * i)/10284);
		cout  << dec << purcent << "%" << "\r";
		cout.flush();
		}
	cout << "\xd" << "100%" <<  endl;

	CloseDevice();
	fd.close();

} else if (param == verif) {
	 cout << "We will verify the flash content " << endl;

	 OpenDevice(0x1556,0x0420);

	 cout << "Start to verify the flash " << endl;

	 for (i=0 ;i < 10284 ;i++) // number of page 10284
		{
		//prepare a page
		for (j=0 ;j<64 ;j++)
			{
			word = 0;
			for (k=0;k<4;k++)
				{
				if (fd.get(byte_c)) {
					//cout << hex << int(byte_c) << endl;
					byte = byte_c & 0xFF;
				 	word = word + (byte * (1<<(k*8)) );
					} else {
					word = word + (0xFF * (1<<(k*8)) );
					}
				}
			buffer_w[j] = word;
			//cout << hex << buffer_w[j] << endl;
			}

		purcent = int((100 * i)/10284);
		cout  << dec << purcent << "%" << "\r";
		cout.flush();

		page_sect[0] = 0x80300000 + (i * 0x100); // set the page to write and prepare read (0x80000000)
		WriteDevice (0XE000,1,page_sect);        // send the address
		WriteDevice (0XE004,1,buffer_w);        // send the request read
		BulkRead(1,64,buffer_r);

		//compare bytes
		for (k=0;k<64;k++) 
			{
			if (buffer_w[k] != buffer_r[k]) {
				cout << "Error :" << buffer_w[k] << "!=" << buffer_r[k] << " at :" << i << " page --" << k << " word "<< endl;
				error++;	
				CloseDevice();
				fd.close();
				return 0;
				}
			}
	}
	cout << "\xd" << "100%" << endl;
	cout << "The flash is well programmed " << endl;

	CloseDevice();
	fd.close();
} else {
	cout << "You didn't provide enough parameters to do something.... nothing was done!" << endl;
        cout << "enter -h parameter " << endl;
}

return 0;
}

